from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

try:
    client = MongoClient("mongodb+srv://Admin:admin123@rasadynamic.5iza5.mongodb.net/test")
    client.server_info()
    print("Connected successfully!!!")
    mydb = client["dynamic_buttons"]
except ConnectionFailure:
   print("Connection failure")

def dbinsert(collection,data:dict):
    try:
        mycol = mydb[collection]
        mycol.insert_one(data)
        print("Data inserted successfully")
    except Exception as e:
        print(e)
    return None

if __name__ == "__main__":
    payload ={
    "question": "What is the difference between a static and dynamic IP?",
    "variations": ["difference between a static and dynamic IP","how is static and dynamic IP different"],
    "keyword": "something",
    "category": "test",
    "response": "this is a test response"
    }
    # print(dbinsert("faq_data",payload))
    mycol = mydb["faq_data"]
    faq ="What is the difference between a laptop and computer"
    data = list(mycol.find({"question":faq},{"_id":0,"response":1,"variations":1}))
    print(data)