import json
from typing import Optional
from bson import json_util
from bson.objectid import ObjectId
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from pydantic import BaseModel
from pymongo import MongoClient, response
from starlette.middleware.cors import CORSMiddleware
from starlette.requests import Request
from starlette.responses import Response
import yaml 
import os
from dotenv import load_dotenv
import requests
from os import walk 
import pandas as pd
import numpy as np
import json
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure

load_dotenv()

RASAURL = os.getenv('RASAURL')
RASAAPI = os.getenv('RASAAPI')
MONGO_URL = os.getenv('MONGO_URL')
templates = Jinja2Templates(directory="templates")

# class Item(BaseModel):
#     name: str
#     data:dict


class Data(BaseModel):
    question: str
    keyword: str
    variations: list
    category: Optional[str] = None
    response: str
    response_type: str


try:
    client = MongoClient(MONGO_URL)
    db = client["dynamic_buttons"]
except Exception as e:
    print(e)
    exit(1)

# initialize the fastapi app
app = FastAPI()

origins = [
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



@app.get("/health")
def health():
    return {"status": "ok"}


@app.get("/home",response_class=HTMLResponse)
def home(request:Request):
    return templates.TemplateResponse("home.html", {"request": request,"message":"This is a demo"})

@app.get("/main_menu")
def main_menu():
    mycol = db["main_menu"]
    data = list(mycol.find({}))
    # print(data)
    data = json.loads(json_util.dumps(data[0]))
    data["_id"] = str(data["_id"]["$oid"])
    # data = loads(data)
    return data

@app.get("/sub_menu/{item}")
def sub_menu(item: str):
    print("*****",item,"******")
    mycol = db["sub_menu"]
    try:
        data = mycol.find({"unique_title": item})
        # print("DATA found--",list(data))
        data = json.loads(json_util.dumps(data[0]))
        print("DATA found--",data)
        data["_id"] = str(data["_id"]["$oid"])
    except:
        data = {"status": "error"}
    return data

@app.post("/main_menu_add")
def main_menu_update(data:dict):
    print("Incoming data---",data,type(data))
    mycol = db["main_menu"]
    try:
        mycol.insert_one(data)
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/main_menu_update/v2")
def main_menu_update(data:dict,id:str,title: Optional[str]= None):
    print("Incoming data---",data,type(data))
    print("ID---",id,type(id))
    print("Title---",title,type(title))
    # data = {"title":"something","payload":"/choose"}
    mycol = db["main_menu"]
    try:
        _id = ObjectId(id)
        # find data
        ed= list(mycol.find({"_id": _id},{"data":1,"_id":0}))
#        print("ED---",ed)
        ed = ed[0]["data"]
#        print(ed,type(ed))
        if title:
            for t in ed:
                if t["title"] == title:
                    t["title"] = data["title"]
                    t["payload"] = data["payload"]
                mycol.update_one({"_id": _id},{"$set": {"data": ed}})
            return {"status": "done"}

        elif title == None:
            ed.append(data)
            mycol.update_one({"_id": _id},{"$set": {"data": ed}})
            return {"status": "done"}
        
    except Exception as e:
        print("error",e)
        return {"status": "failed"}

@app.post("/main_menu_delete")
def main_menu_delete(id:str,data:dict):
    mycol = db["main_menu"]
    try:
        _id = ObjectId(id)
        ed= list(mycol.find({"_id": _id},{"data":1,"_id":0}))
#        print("ED---",ed)
        ed = ed[0]["data"]
        # for t in ed:
        #     if t["title"] == title:
        #         print(t)
        ed.remove(data)
        mycol.update_one({"_id": _id},{"$set": {"data": ed}})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/main_menu_update")
def main_menu_update(data:dict):
    print("Incoming data---",data,type(data))
    mycol = db["main_menu"]
    try:
        mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"data": data["data"], "utterance": data["utterance"]}})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/sub_menu_update")
def main_menu_update(data:dict):
    print("Incoming data---",data,type(data))
    mycol = db["sub_menu"]
    try:
        _id = ObjectId(data["_id"])
        mycol.update_one({"_id": _id},{"$set": {"data": data["data"]}})
        return {"status": "done"}
    except:
        return {"status": "failed"}

@app.post("/sub_menu_update/v2")
def main_menu_update(data:dict,id:str,title: Optional[str]= None):
    print("Incoming data---",data,type(data))
    print("ID---",id,type(id))
    print("Title---",title,type(title))
    # data = {"title":"something","payload":"/choose"}
    mycol = db["sub_menu"]
    try:
        _id = ObjectId(id)
        # find data
        ed= list(mycol.find({"_id": _id}))
        print("ED---",ed)
        ed = ed[0]
#        print(ed,type(ed))
        if title:
            for t in ed["data"]:
                if t["title"] == title:
                    print("dsfgaceegevchfsdhtfctr")
                    t["title"] = data["title"]
                    t["payload"] = data["payload"]
                mycol.update_one({"_id": _id},{"$set": {"data": ed["data"]}})
            return {"status": "done"}

        elif title == None:
            if "title" in data:
                ed["data"].append(data)
            if "utterance" in data: 
                ed["utterance"] = data["utterance"]
            if "category" in data:
                ed["category"] = data["category"]
            # print(ed["data"],ed["utterance"])
            mycol.update_one({"_id": _id},{"$set": {"data": ed["data"],"utterance":ed["utterance"],"category":ed["category"]}})
            return {"status": "done"}
        
    except Exception as e:
        print("error",e)
        return {"status": "failed"}

@app.post("/sub_menu_delete")
def sub_menu_delete(id:str,data:dict):
    mycol = db["sub_menu"]
    try:
        _id = ObjectId(id)
        ed= list(mycol.find({"_id": _id},{"data":1,"_id":0}))
#        print("ED---",ed)
        ed = ed[0]
        # for t in ed:
        #     if t["title"] == title:
        #         print(t)
        if "title" in data:

            ed["data"].remove(data)
            mycol.update_one({"_id": _id},{"$set": {"data": ed["data"]}})
        elif "utterance" in data: 
            # ed["utterance"].remove(data)
            mycol.update_one({"_id": _id},{"$set": {"utterance":""}})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/sub_menu_deleteall")
def submenu_deleteall(id:str):
    mycol = db["sub_menu"]
    _id = ObjectId(id)
    try:
        mycol.delete_one({"_id": _id})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/sub_menu_add")
def main_menu_update(data:dict):
    print("Incoming data---",data,type(data))
    mycol = db["sub_menu"]
    try:
        mycol.insert_one(data)
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.get("/college_details{item}")
def get_colleges(item:str):
    mycol = db["college_details"]
    data = mycol.find({"unique_title": item})
    # data = list(mycol.find({}))
    data = json.loads(json_util.dumps(data))
    # data["_id"] = str(data["_id"]["$oid"])
    return data

@app.post("/college_update")
def college_update(data:dict):
    mycol = db["college_details"]
    try:
        if "streams_available" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"streams_available": data["streams_available"]}})
        if "college_name" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"college_name": data["college_name"]}})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/college_add")
def college_update(data:dict):
    mycol = db["college_details"]
    try:
        mycol.insert_one(data)
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"} 

@app.post("/college_delete")
def college_update(id:str):
    mycol = db["college_details"]
    _id = ObjectId(id)
    try:
        mycol.delete_one({"_id": _id})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.get("/school_details{item}")
def get_schools(item:str):
    mycol = db["school_details"]
    data = mycol.find({"unique_title": item})
    # data = list(mycol.find({}))
    data = json.loads(json_util.dumps(data))
   # data["_id"] = str(data["_id"]["$oid"])
    return data
@app.post("/school_updates")
def school_update(data:dict):
    mycol = db["school_details"]
    try:
        if "states_list" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"states_list": data["states_list"]}})
        if "prefecture" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"prefecture": data["prefecture"]}})

        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/school_add")
def school_add(data:dict):
    mycol = db["school_details"]
    try:
        mycol.insert_one(data)
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/school_delete")
def college_update(id:str):
    mycol = db["school_details"]
    _id = ObjectId(id)
    try:
        mycol.delete_one({"_id": _id})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

#-------------------------------------------------------FAQ API's-------------------------------------------------------

@app.get("/api/faqData/all")
def get_faq_all():
    """
    Get all faq data as a list
    """
    mycol = db["faq_data"]
    data = list(mycol.find({},{"_id":0,"question":1}))
    x = [i["question"] for i in data if i.get("question")]
    print(x)
    response ={"success":True,"data":x}
    data = json.loads(json_util.dumps(response))
    return data

@app.get("/api/faqData")
def get_faq(faq:str):
    """Get single faq data
    
    Example:- <uri>/api/faqData/ما هي شروط القبول الموحدة

    Args:
        faq (str): faq name 
    
    Returns:
        success: true/false
        
        data: faq data
    """
    print("Incoming data---",faq,type(faq))
    mycol = db["faq_data"]
    try:
        data = list(mycol.find({"question":faq},{"_id":1,"keyword":1,"variations":1,"response":1,"response_type":1}))
        x=data[0]
        response ={"success":True,"data":x}
        data = json.loads(json_util.dumps(response))
    except Exception as e:
        print(e)
        response ={"success":False,"data":None}
    return data


@app.post("/api/faqData/add")
def faq_add(data:dict,id:Optional[str] = None):
    """Adds new faq

    Args:
        data (dict): {
            "question": "What is the difference between a static and dynamic IP?",
            "variations": ["difference between a static and dynamic IP","how is static and dynamic IP different"],
            "keyword": "something",
            "category": "test",
            "response_type": "text",
            "response": "this is a test response"
        }

    Returns:
        success: true/false
    """
    mycol = db["faq_data"]
    try:
        if id:
            _id = ObjectId(id)
            ed= mycol.find({"_id": _id})
            print(ed[0])
            # if ed[0]["_id"] == _id:
            mycol.update_one({"_id": _id},{"$set": {"variations": data["variations"],"response_type":data["response_type"],"response":data["response"],"question":data["question"]}})
            return {"success": True}
        else:
            keyword = data.get("keyword")
            if keyword:
                data["faq_intent"] = "faq/"+keyword
                mycol.insert_one(data)
                return {"success": True}
         
    except Exception as e:
        print(e)
        return {"success": False}


# @app.post("/api/faqData/save")
# def faq_update(data:dict):
#     """ Update faq 
#     """
#     mycol = db["faq"]
#     try:
#         keyword = data.get("keyword")
#         response = data.get("response")
#         response_type= data.get("response_type")
#         mycol.update_one({"keyword":keyword}, {"$set": {"variations": data["variations"],"response": response,"response_type":response_type,"keyword": keyword}})
#         return {"success": True}
#     except Exception as e:
#         print(e)
#         return {"success": False}

@app.delete("/api/faqData/delete")
def faq_delete(faq:str):
    """ Delete faq 
    """
    mycol = db["faq_data"]
    try:
        mycol.delete_one({"question":faq})
        return {"success": True}
    except Exception as e:
        print(e)
        return {"success":False}

@app.post("/api/faqData/train")
def faq_train():
    mycol = db["faq_data"]
    data = list(mycol.find({}))

    with open(RASAURL+'domain.yml', "r",encoding='utf-8') as domainfile:
        domainData = yaml.safe_load(domainfile)
        for i in data:
            domainName = 'utter_faq/'+i.get("faq_intent")[4:]
            if domainName not in domainData["responses"]:
                resText = {'text':i.get("response")}
                domainData["responses"][domainName] = [resText]
            else:
                domainData['responses'][domainName][0]['text'] = i.get("response")

    with open(RASAURL+'data/faq.yml', "r",encoding='utf-8') as fileFaq:
        faqData = yaml.safe_load(fileFaq)
        print("faqData  ",faqData)
        
        for i in data:
            faqName = 'faq/'+i.get("faq_intent")[4:]
            faqs = {'intent':faqName}
       
            faqKey = faqData["nlu"]
            # output = '\n'.join("-\n "+allFaq for allFaq in i.get("variations"))
            # print(output)
            allFaq = ""
            for myfaq in i.get("variations"):
                # print(i.get("variations"))
                if i.get("variations").index(myfaq) == 0 :
                    allFaq += "- "+myfaq
                else:
                    allFaq += "\n- "+myfaq
            print(allFaq)
            faqList = []
            
            for count in range(len(faqKey)):
                faqList.append(faqKey[count]['intent'])
                if (faqKey[count]['intent'] == 'faq/'+i.get("faq_intent")[4:]):
                    faqKey[count]['intent'] = 'faq/'+i.get("faq_intent")[4:]
                    faqKey[count]["examples"] = allFaq

            
            if faqName not in faqList:
        
                faqs.update({"examples":allFaq})
                faqData["nlu"].append(faqs)
    print(faqData)    
    with open(RASAURL+"domain.yml", "w",encoding='utf-8') as file:
        yaml.dump(domainData, file, allow_unicode=True)

    with open(RASAURL+"data/faq.yml", "w",encoding='utf-8') as file:
        yaml.dump(faqData, file, allow_unicode=True, sort_keys=False)

    exec = os.system('bash -i  train.sh')

@app.post("/api/faqData/trained/model")
def faq_useTrainedModel(model:str):
    try:
        models = RASAURL+"models/"+model
        modelUrl= RASAAPI+'model/'
        print(modelUrl,models)
        querystring = {"model_file": models}
        headers = {"content-type": "application/json"}
        
        response = requests.put(modelUrl, data=json.dumps(querystring), headers=headers)
        return {"success":True,"message":"Model Activated"}
    except Exception as e:
        print(e)
        return {"success":False}

@app.get("/api/faqData/modelList")
def faq_modelList():
    try:
        modelFolder = RASAURL+"models/"
        filenames = next(walk(modelFolder), (None, None, []))[2]
        return{"success":True,"data":filenames}
    except Exception as e:
        print(e)
        return {"success":False}

#-----------------------------Indicators and statistics-----------------------------------------------------------

@app.get("/statistic details{item}")
def get_colleges(item:str):
    mycol = db["statistic"]
    data = mycol.find({"unique_title": item})
    # data = list(mycol.find({}))
    data = json.loads(json_util.dumps(data))
    # data["_id"] = str(data["_id"]["$oid"])
    return data

@app.post("/statistic_update")
def college_update(data:dict):
    mycol = db["statistic"]
    try:
        if "session_list" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"session_list": data["session_list"]}})
        if "student_type" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"student_type": data["student_type"]}})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/statistic_add")
def college_update(data:dict):
    mycol = db["statistic"]
    try:
        mycol.insert_one(data)
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"} 

@app.post("/statistic_delete")
def college_update(id:str):
    mycol = db["statistic"]
    _id = ObjectId(id)
    try:
        mycol.delete_one({"_id": _id})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

#----------------------------Qualifcation majors---------------------------------------------------

@app.get("/majors_details{item}")
def get_majors(item:str):
    mycol = db["qualification_major"]
    data = mycol.find({"unique_title": item})
    # data = list(mycol.find({}))
    data = json.loads(json_util.dumps(data))
    # data["_id"] = str(data["_id"]["$oid"])
    return data

@app.post("/majors_update")
def majors_update(data:dict):
    mycol = db["qualification_major"]
    try:
        if "major_list" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"major_list": data["major_list"]}})
        if "major_type" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"major_type": data["major_type"]}})
        if "compatible_type" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"compatible_type": data["compatible_type"]}})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/majors_add")
def majors_update(data:dict):
    mycol = db["qualification_major"]
    try:
        mycol.insert_one(data)
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"} 

@app.post("/majors_delete")
def majors_update(id:str):
    mycol = db["qualification_major"]
    _id = ObjectId(id)
    try:
        mycol.delete_one({"_id": _id})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}


#----------------------------Tst Interview ProgramCodes---------------------------------------------------

@app.get("/appointmentbycode_details{item}")
def get_appointmentbycode(item:str):
    mycol = db["test_code"]
    data = mycol.find({"unique_title": item})
    # data = list(mycol.find({}))
    data = json.loads(json_util.dumps(data))
    # data["_id"] = str(data["_id"]["$oid"])
    return data

@app.post("/appointmentbycode_update")
def appointmentbycode_update(data:dict):
    mycol = db["test_code"]
    try:
        if "code" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"code": data["code"]}})
        if "details" in data:
            mycol.update_one({"_id": ObjectId(data["_id"])}, {"$set": {"details": data["details"]}})
    except Exception as e:
        print(e)
        return {"status": "failed"}

@app.post("/appointmentbycode_add")
def appointmentbycode_update(data:dict):
    mycol = db["test_code"]
    try:
        mycol.insert_one(data)
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"} 

@app.post("/appointmentbycode_delete")
def appointmentbycode_update(id:str):
    mycol = db["test_code"]
    _id = ObjectId(id)
    try:
        mycol.delete_one({"_id": _id})
        return {"status": "done"}
    except Exception as e:
        print(e)
        return {"status": "failed"}

#--------------------------------- add college by excel -----------------------------------------

@app.post("/addcolleges_by_excel")
def add_colleges_by_excel(excel_url:str,category:str,unique_title:str ):
    df = pd.read_excel(str(excel_url), index_col=None,)
    if unique_title == "Inside Sultanate":
        colleges = df['COLLEGENAME'].tolist()
        df_college = pd.DataFrame(df)
        # print(df_college["COLLEGENAME"].iloc[5])

        colleges = []
        # print(sheet.nrows)
        for i in range(len(df)):
            # print(sheet.cell_value(i,0))
        #    print(sheet.cell_value(i,8))
            colleges.append(df_college["COLLEGENAME"].iloc[i])
        arr = np.array(colleges)
        college_list = np.unique(arr)
        unq_college = []
        for lst in college_list:
            programs = []
            progr = []
            for i in range(len(df)):
                if df_college["COLLEGENAME"].iloc[i]==lst:
                    print(df_college["COLLEGENAME"].iloc[i],df_college["PROGRAMTYPE"].iloc[i])
                    college_detail = {
                            "category":category,
                            "unique_title":unique_title,
                            "type":"sub menu",
                            "college_name":lst,
                            "college_type":df_college["COLLEGETYPE"].iloc[i] ,
                            "program_type":df_college["PROGRAMTYPE"].iloc[i] ,
                            "utterance":"",
                            "country":""
                            }

                    #    unq_college.append(college_detail)

            # for i in range(1,sheet.nrows):
            #     if sheet.cell_value(i,4)==lst:
                    programs.append(df_college["STREAM"].iloc[i])
        #            program_code.append(sheet.cell_value(i,0))
                # break

            parr = np.array(programs)
            program_list = np.unique(parr)
            unq_programs = []
            for plst in program_list:
                program_det = {"stream_name":plst,
                        }
                program_code = []
                progr_code = []
                for i in range(len(df)):
                    if df_college["STREAM"].iloc[i]==plst and df_college["COLLEGENAME"].iloc[i]==lst:
                        program_code.append(df_college["PROGRAMCODE"].iloc[i])
        #        print(program_code)
                    for pcode in program_code:
                        if pcode==df_college["PROGRAMCODE"].iloc[i]:
                            program_detail = {"program_code":pcode,
                            "details":df_college["PROGRAMDETAILS"].iloc[i]
                                    }
                            progr_code.append(program_detail)
                program_det['program_list'] = progr_code
                progr.append(program_det)


        #    for plist,pcode in zip(programs,program_code):
        #        program_detail = {"stream_name":plist,
        #                          "stream_option":program_code.index(pcode)+1,
        #                          "program_code":pcode
        #                }
        #        progr.append(program_detail)
            college_detail['streams_available'] = progr
            unq_college.append(college_detail)

    if unique_title == "Outside Sultanate":
        countries = df['COUNTRY'].tolist()
        df_countries = pd.DataFrame(df)

        countries = []
        # print(sheet.nrows)
        for i in range(len(df)):
            # print(sheet.cell_value(i,0))
        #    print(sheet.cell_value(i,8))
            countries.append(df_countries["COUNTRY"].iloc[i])
        arr = np.array(countries)
        countries_list = np.unique(arr)
        unq_college = []
        for lst in countries_list:
            programs = []
            progr = []
            for i in range(len(df)):
                if df_countries["COUNTRY"].iloc[i]==lst :
                    countries_detail = {
                            "category":category,
                            "unique_title":unique_title,
                            "type":"sub menu",
                            "college_name":df_countries["COLLEGENAME"].iloc[i],
                            "college_type":df_countries["COLLEGETYPE"].iloc[i] ,
                            "program_type":df_countries["PROGRAMTYPE"].iloc[i] ,
                            "utterance":"",
                            "country":lst
                            }
                    # countries_detail['streams_available'] = programs
             

                   
                    #    unq_college.append(college_detail)

            # for i in range(1,sheet.nrows):
            #     if sheet.cell_value(i,4)==lst:
                    programs.append(df_countries["STREAM"].iloc[i])
        #            program_code.append(sheet.cell_value(i,0))
                # break
            
            parr = np.array(programs)
            program_list = np.unique(parr)
            unq_programs = []
            for plst in program_list:
                program_det = {"stream_name":plst,
                        }
                program_code = []
                progr_code = []
                for i in range(len(df)):
                    if df_countries["STREAM"].iloc[i]==plst and df_countries["COUNTRY"].iloc[i]==lst:
                        program_code.append(df_countries["PROGRAMCODE"].iloc[i])
        #        print(program_code)
                    for pcode in program_code:
                        if pcode==df_countries["PROGRAMCODE"].iloc[i]:
                            program_detail = {"program_code":pcode,
                            "details":df_countries["PROGRAMDETAILS"].iloc[i]
                                    }
                            progr_code.append(program_detail)
                program_det['program_list'] = progr_code
                progr.append(program_det)


    #    for plist,pcode in zip(programs,program_code):
    #        program_detail = {"stream_name":plist,
    #                          "stream_option":program_code.index(pcode)+1,
    #                          "program_code":pcode
    #                }
    #        progr.append(program_detail)
            countries_detail['streams_available'] = progr
            
            unq_college.append(countries_detail)
    # print(unq_college)
    # with open('insidesultanatedata.json', 'w', encoding='utf8') as fp:
    #     json.dump(unq_college, fp, indent=4, ensure_ascii=False)

    for data in unq_college:
        print("data.....",data)
        mycol = db["college_details"]
      
        mycol.insert_one(data)
    return {"status": "done"}
        # except Exception as e:
            # print(e)
            # return{"status", "failed"} 
